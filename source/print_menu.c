/*
print_menu.cpp
输出菜单的子程序
*/
#include<stdio.h>
#include"rmmc.h"
void star(int m)
{//循环输出m*2个“*”
	int star_m = 0;
	while (star_m < m)
	{
		printf_s("**");
		star_m++;
	}
}
void logo()
{//字符画以及标题
	printf_s("\n*   _____    __  __   __  __    _____  *\n*  |  __ \\  |  \\/  | |  \\/  |  / ____| *\n*  | |__) | | \\  / | | \\  / | | |      *\n*  |  _  /  | |\\/| | | |\\/| | | |      *\n*  | | \\ \\  | |  | | | |  | | | |____  *\n*  |_|  \\_\\ |_|  |_| |_|  |_|  \\_____| *\n*      相 对 分 子 质 量 计 算 器      *\n*  Relative Molecular Mass Calculator  *\n");
}
void menuoutput(int mode)
{
	star(20);//输出40个*
	logo();//打印log字符
	star(20);//输出40个*
	printf_s("\n欢迎使用相对分子质量计算器（控制台程序）\n主菜单：\t1.开始计算\t2.模式选择\t3.模式设置\t4.使用教学\t5.关于本程序\n");//主菜单
	modeif(mode);//显示当然模式
	printf_s("请输入菜单序号，按回车确定:");
}
int modeswitch_menu(int mode,int status)
{
	int i;
	printf_s("模式选择：\n");
	modeif(mode);
	printf("选择你想要切换的模式:\t1.高中模式\t2.科研模式\t0.退回主菜单\n输入模式的序号,按回车确认:");
	scanf_s("%d", &i);
	switch (i)
	{
	case 1:mode = 1; break;
	case 2:mode = 2; break;
	case 0:status = 1; restart(mode, status); break;
	default:printf("序号错误,请重新输入!"); modeswitch_menu(mode,status);break;
	}
	return mode,status;
}
