#include<stdio.h>
#include<stdlib.h>
#include"rmmc.h"
int mode=2,status=0;
/*
mode:模式变量,1为高中模式,2为科研模式
status:用来判断页面的状态的变量,终端是否需要清屏和重新打印菜单的状态返回码,0为不需要,1为需要
*/
int restart(int mode, int status)
{
	if (status==0)
	{
		return status;
	}
	else
	{
		system("cls");
		menuoutput();	//打印菜单
		input(mode,status);
		status = 0;
		return status;
	}
}
int main()
{
	menuoutput(mode);	//打印菜单
	input(mode,status);
}